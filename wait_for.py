# Importing library: time
import time


def wait_for(sec=60):
    print "I am going to wait for {} seconds.".format(sec)
    time.sleep(sec)
    print "{} seconds finished".format(sec)


if __name__ == '__main__':
    sec = 60
    wait_for(sec)
