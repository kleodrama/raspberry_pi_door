# importing the library for GPIOs
import RPi.GPIO as GPIO
# Importing library: time
import time


def led(pin=12):
    GPIO.setup(pin, GPIO.OUT)
    # Turn on led
    GPIO.output(pin,GPIO.HIGH)
    print "LED on"
    # Sleep for 1 sec
    time.sleep(1)
    # turn of led
    GPIO.output(pin,GPIO.LOW)
    print "LED off"


if __name__ == '__main__':
    # Set GPIO nlabeling mode
    GPIO.setmode(GPIO.BCM)
    # Set warnings to FALSE
    GPIO.setwarnings(False)
    pin = 12
    led(pin)
