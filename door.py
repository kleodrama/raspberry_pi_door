# importing the library for GPIOs
import RPi.GPIO as GPIO
# Importing library: time
import time
# import subprocess
import threading

import sys
sys.path.append('/home/pi/Desktop/door/raspberry_pi_door/')

from if_is_night import is_night
from light import light


def open_door(pin=26):
    if is_night():
        threading_light = threading.Thread(target=light)
        threading_light.start()
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)
    print 'sending signal'
    time.sleep(1.5)
    GPIO.output(pin, GPIO.HIGH)
    print 'signal stopped'


if __name__ == '__main__':
    # Set GPIO labeling mode
    GPIO.setmode(GPIO.BCM)
    # Set warnings to FALSE
    GPIO.setwarnings(False)
    pin = 26
    open_door(pin)

