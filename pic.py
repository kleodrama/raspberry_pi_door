import picamera
from datetime import datetime
import subprocess
import pytz




def take_picture():
    try:
        camera = picamera.PiCamera()
    except:
        print 'could not connect'
        quit()
    camera.vflip = True
    camera.hflip = True

    camera.start_preview()
    camera.annotate_text = datetime.now(pytz.timezone("Europe/Athens")).strftime("%a %d %b %Y %H:%M:%S")
    camera.capture('image.jpg')
    print 'Picture saved'
    camera.stop_preview()

def uplaod_picture():
    print 'uploading'
    subprocess.call(['sh', '/home/pi/Desktop/door/raspberry_pi_door/transfer.sh'])
    print 'uploaded'


if __name__ == '__main__':
    take_picture()
    uplaod_picture()
