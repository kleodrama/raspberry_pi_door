# README #

Raspberry pi project to control my garage door and light.

### What is this repository for? ###

* open/close garage door
* turn on the light if it is night
* control over the internet (via django app that sends a postgreSQL notifiction to a channel that raspberry pi listens to)
* control via sensors connected to raspberry pi (distance sensor, rgb color sensor, relays, rfid reader)