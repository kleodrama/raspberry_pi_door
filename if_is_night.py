
import smbus
import time

def is_night():
    bus = smbus.SMBus(1)
    bus.write_byte(0x29, 0x80 | 0x12)
    ver = bus.read_byte(0x29)
    # version # should be 0x44
    if ver == 0x44:
        print "Device found\n"
        for i in range(4):
            bus.write_byte(0x29, 0x80|0x00) # 0x00 = ENABLE register
            bus.write_byte(0x29, 0x01|0x02) # 0x01 = Power on, 0x02 RGB sensors enabled
            bus.write_byte(0x29, 0x80|0x14) # Reading results start register 14, LSB then MSB
            data = bus.read_i2c_block_data(0x29, 0)
            clear = data[1] #<< 8 | data[0]
            red = data[3] #<< 8 | data[2]
            green = data[5] #<< 8 | data[4]
            blue = data[7] #<< 8 | data[6]
            crgb = "C: %s, R: %s, G: %s, B: %s\n" % (clear, red, green, blue)
            print crgb
            time.sleep(0.2)
        if clear < 15:
            return True
        else:
            return False
    else:
        print "Device not found\n"
        return False


if __name__ == '__main__':
    value = is_night()
    print value
    print 40 * '-'
    if value:
        print 'It seems to be night'
    else:
        print 'It seems to be day'
    print 40 * '-'
