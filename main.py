from settings import *
import threading
import time
import RPi.GPIO as GPIO
import select
import psycopg2
import psycopg2.extensions
import urllib2

import sys
sys.path.append('/home/pi/Desktop/door/raspberry_pi_door/')

from distance import distance_loop
from listen import listen_loop
from door import open_door
# from wait_for import wait_for


# Set GPIO labeling mode
GPIO.setmode(GPIO.BCM)
# Set warnings to FALSE
GPIO.setwarnings(False)
# Set pin
pin_door = 22


distance_threading = threading.Thread(target=distance_loop)
distance_threading.start()

listen_threading = threading.Thread(target=listen_loop)
listen_threading.start()

# wait_threading = threading.Thread(target=wait_for)
# wait_threading.start()

while True:
    time.sleep(1)
    if not distance_threading.is_alive():
        distance_threading.start()
    if not listen_threading.is_alive():
        listen_threading.start()
    time.sleep(1)
