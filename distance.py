

import RPi.GPIO as GPIO
import time
# import subprocess
import threading

import sys
sys.path.append('/home/pi/Desktop/door/raspberry_pi_my_door/')

from door import open_door


def distance_loop(pin=22):
    # set 'pin' to input mode
    GPIO.setup(pin, GPIO.IN)
    while True:
        state = GPIO.input(pin)
        if state == 1:
            print 'nothing found'
        else:
            print 'found something'
            open_door()
        time.sleep(0.2)


if __name__ == '__main__':
    # Set GPIO labeling mode
    GPIO.setmode(GPIO.BCM)
    # Set warnings to FALSE
    GPIO.setwarnings(False)
    # Set pin
    pin_door = 22

    distance_loop(pin_door)
