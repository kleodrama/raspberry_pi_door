
from settings import *
import time

import select
import psycopg2
import psycopg2.extensions
# import subprocess
import threading
import datetime ##############

import sys
sys.path.append('/home/pi/Desktop/door/raspberry_pi_door/')

# from if_is_night import is_night
# from light import light
from door import open_door
from pic import take_picture, uplaod_picture

import urllib2

def listen_loop():
    while True:
        start = datetime.datetime.now()
        # check if can connect to server
        for i in range(20):
            try:
                urllib2.urlopen('http://' + HOST, timeout=1)
                print 'can connect'
            except urllib2.URLError as err:
                print '----{}----'.format(i)
                print 'trying again to connect. {}'.format(err)
                time.sleep(10)
                continue
            break

        DSN = "host={} dbname={} user={} password={}".format(HOST, DBNAME, USER, PASSWORD)
        print "Waiting for notifications on channel 'test'"

        conn = psycopg2.connect(DSN)
        conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

        curs = conn.cursor()
        curs.execute("LISTEN test;")

        while True:
            now = datetime.datetime.now()
            if select.select([conn], [], [], 4) == ([], [], []):
                print "Starting again"
            else:
                conn.poll()
                while conn.notifies:
                    notify = conn.notifies.pop(0)
                    print "Got NOTIFY:", notify.pid, notify.channel, notify.payload
                    if notify.payload == 'please open the door':
                        print 'i got notify to open the door'
                        open_door()
                    elif notify.payload == 'please take a photo':
                        print 'i got notify to take a picture'
                        picture_threading = threading.Thread(target=take_picture)
                        picture_threading.start()
                        upload_threading = threading.Thread(target=uplaod_picture)
                        upload_threading.start()
                    else:
                        print 'i did not understand the notification.'
            if (now - start).seconds > 60:
                print (now - start).seconds, 'seconds'
                break
        curs.execute("UNLISTEN test;")
        curs.close()
        conn.close()
        continue


if __name__ == '__main__':
    listen_loop()