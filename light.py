# importing the library for GPIOs
import RPi.GPIO as GPIO
# Importing library: time
import time


def light(pin=19):
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, GPIO.LOW)
    print 'LIGHT ON'
    time.sleep(120)
    GPIO.output(pin, GPIO.HIGH)
    print 'LIGHT OFF'



if __name__ == '__main__':
    GPIO.setmode(GPIO.BCM)
    pin = 19
    light(pin)
